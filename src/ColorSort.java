public class ColorSort {

   enum Color {red, green, blue};
   
   public static void main (String[] param) {
      // for debugging
	   System.out.println("debugging");
   }
   //The reordering function, assuming the beginning, half and ending of the array with start, middle and end.
   public static void reorder (Color[] balls) {
	   int beginning = 0;
	   int middle = 0;
	   int end = balls.length -1;
	   
	   //1 - Red, 2 - Green, 3 - Blue
       //Method to rearrange the array
	   while (middle <= end)
           switch (balls[middle]) {
               case red:
            	   reorderit(balls, beginning++, middle++); //if its red we require the function reorderit to change places from start to middle and counting +1 to it.
                   break;
               case green:
                   middle++; //since green should be in the middle we do not replace it with anything.
                   break;
               case blue:
            	   reorderit(balls, middle, end--); //if its blue we request the reorderit function to replace blue in middle with end and -1 from it.
                   break;
           }
   }
   //Creating the reordering array with temporary array array to hold current one, so temporary array will hold the array1, array1 will take array2 and then array2 will take the temparray which was array
   //With those moves it replace the arr1 with arr2 and arr2 with arr1
   
   private static void reorderit(Color[] newColor, int arr1, int arr2) {
       Color temparray = newColor[arr1];
       newColor[arr1] = newColor[arr2];
       newColor[arr2] = temparray;
   }
}